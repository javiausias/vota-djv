       $(function () {
        	
            $.ajax({
                type: 'post',
        		url:'php/enquestes.php',
                dataType: 'JSON'
        		}).done(function(datos){
					console.log(datos);
					$.each(datos, function(index, itemData){
                    	$('#resposta').append(`
							<div class='enquesta'>
								<div class='pregunta'>${itemData.pregunta}</div>
								<div class='dataInici'>${itemData.data_inici}</div>
								<div class='dataFinal'>${itemData.data_final}</div>
                                <div class='quesito'><canvas id="${itemData.id}"></canvas></div>
							</div>
						`);
                        pintaQueso(itemData.id, 12, 21);
					});
                });


            $('form').on('submit', function (e) {
                e.preventDefault();

                $.ajax({
                    type: 'post',
                    url: 'php/prova.php',
                    data: $('form').serialize(),
                    dataType: 'JSON',
                    success: function (response) {
                        $.each(response,function(index,itemData){
                            console.log(itemData);
                        });
                        
						if(response){
                            $('#resposta').html("Hola "+response[0].username);
                            console.log(response[0].username);
                        }
                        else {
                            $('#resposta').html("Error !");
                            console.log(response);
                        }
                    }
                });

            });

        });
