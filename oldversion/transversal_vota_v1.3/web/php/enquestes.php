 <?php
	require('../secure/login.php');

	$conn = mysqli_connect($servername, $username, $password, $dbname);

	if(!$conn){
		die('Error de Conexion: '. mysqli_connect_errno());
	}

	$query = "SELECT * FROM enquesta where destacada = true";
//	$query2 = "SELECT id_enquesta, valor, count(*) FROM resposta GROUP BY id_enquesta, valor";
//	$query3 = 'SELECT COUNT(valor) AS no FROM enquesta NATURAL JOIN resposta WHERE enquesta.id = resposta.id_enquesta AND valor = 0 AND id_enquesta =1';

	$pruebasi = "SELECT count(*) AS si FROM enquesta INNER JOIN resposta WHERE enquesta.id = resposta.id_enquesta AND valor = 1 GROUP BY id_enquesta";
	$pruebano = "SELECT count(*) AS no FROM enquesta INNER JOIN resposta WHERE enquesta.id = resposta.id_enquesta AND valor = 0 GROUP BY id_enquesta";
	$resultado = mysqli_query($conn, $query);
	$resultsi  = mysqli_query($conn, $pruebasi);
	$resultno  = mysqli_query($conn, $pruebano);

	$preg = array(
		'data' => array(
					'enquesta' => array(),
					'respostes' =>  array()
				), 
	);

	if(!$resultado){
		die("Error");
	}
	else{

		while($data = mysqli_fetch_object($resultado)) {

			$si = mysqli_fetch_assoc($resultsi);
			$no = mysqli_fetch_assoc($resultno);

			
			$sino =  (object)array('si' => $si, 'no' => $no);
			array_push($preg['data']['enquesta'], $data);
			array_push($preg['data']['respostes'], $sino);
	    }
	}

	print json_encode($preg);
	
	mysqli_free_result($resultado);
	mysqli_close($conn);
?> 
