  <?php

 	require('../secure/login.php');

	$conn = new mysqli($servername, $username, $password, $dbname);


	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}

	$sql = "SELECT * FROM preguntes";
  	$result = mysqli_query($conn, $sql);
  	$preguntes = array(
  		'preguntes' => array(
  			'pregunta' => '',
  			'destacada' => ''
  		),
  	); 
  	while ($row = mysqli_fetch_array($result)) {
  		array_push($preguntes->preguntes['pregunta'], $row['pregunta']);
  		array_push($preguntes->preguntes['destacada'], $row['destacada']);
  	}

	
/*
	$objecte = json_encode(array("<div class='quesitos'><h3>Hola amigos que tal estais?</h3></div>",
								 "<div class='quesitos'><h3>Hola amigos que tal estais?</h3></div>",
								 "<div class='quesitos'><h3>Hola amigos que tal estais?</h3></div>"), JSON_FORCE_OBJECT);
*/


	print json_encode($preguntes);


	mysqli_close($conn);

/*
{
  "id": "1",
  "username": "Victor",
  "password": "Victor1\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000",
  "admin": "0"
}*/

?> 
