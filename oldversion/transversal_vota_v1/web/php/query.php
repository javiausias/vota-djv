<?php
	require('../secure/login.php');

	function query($query){

		$conn = mysqli_connect($servername, $username, $password, $dbname);

		if(!$conn){
			die('Error de Conexion: '. mysqli_connect_errno());
		}

		$resultado = mysqli_query($conn, $query);
		return $resultado;

	}
