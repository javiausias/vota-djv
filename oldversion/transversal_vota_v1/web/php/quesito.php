<?php
	require('../secure/login.php');

	$conn = mysqli_connect($servername, $username, $password, $dbname);

	if(!$conn){
		die('Error de Conexion: '. mysqli_connect_errno());
	}

	$pruebasi = "SELECT count(*) AS si FROM enquesta INNER JOIN resposta WHERE enquesta.id = resposta.id_enquesta AND valor = 1 GROUP BY id_enquesta";
	$pruebano = "SELECT count(*) AS no FROM enquesta INNER JOIN resposta WHERE enquesta.id = resposta.id_enquesta AND valor = 0 GROUP BY id_enquesta";

	$resultsi  = mysqli_query($conn, $pruebasi);
	$resultno  = mysqli_query($conn, $pruebano);
