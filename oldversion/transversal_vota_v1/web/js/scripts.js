$(function () {
    $.ajax({
        type: 'post',
		url:'php/enquestes.php',
        dataType: 'JSON'
		}).done(function(datos){
			//console.log(datos);
			//console.log(datos.data.enquesta);
			//console.log(datos.data.respostes);
			let cont = 0;
			let respostes = datos.data.respostes;

			$.each(datos.data.enquesta, function(index, itemData){
            	$('#resposta').append(`
					<div class='enquesta' >
						<div class='pregunta'>${itemData.pregunta}</div>
						<div class='dataInici'>${itemData.data_inici}</div>
						<div class='dataFinal'>${itemData.data_final}</div>
                        <div class='quesito'><canvas id="${itemData.id}"></canvas></div>
					</div>
				`);

				//console.log(Object.values(numRespuestas[cont]));

				if(respostes[cont].si == undefined && respostes[cont].no == undefined){
					pintaQueso(itemData.id, 0, 0);
				}else if(respostes[cont].si == undefined){
					pintaQueso(itemData.id, 0, Object.values(respostes[cont].no));
				}else if(respostes[cont].no == undefined){
					pintaQueso(itemData.id, Object.values(respostes[cont].si), 0);
				}else
					pintaQueso(itemData.id, Object.values(respostes[cont].si), Object.values(respostes[cont].no));

				cont++;

             });
    });
   

    //funcion que se ejecuta cuando haces submit
    $('form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                    type: 'post',
                    url: 'php/registre_usuari.php',
                    data: $('form').serialize(),
                    dataType: 'json',
                success: function (response) {
                        console.log(response.admin);
                //condiciones para derivar a la parte admin o a la parte usuario
                        if(response.admin == 1){
                    //redireccionamiento
                            window.location.href ="/admin";
                        }
                        else{
	                    //Aqui ocultamos el formulario una vez hecho submit y comprobado que el usuario se encuentra en la BBDD 
	                    $('form').hide();
	                    //Invocacion de la funcion para pintar todas las encuestas si no es admin, pasando por parametro el objeto usuario.
	                            llamada(response);
	                        }
	                    }
         
            });

    });
    
    //funcion que es invocada si el usuario no es admin, que recibe por parametro el objeto usuario
    function llamada(resp){
        //En el div resposta pintaremos dentro lo que necesitemos
            $('#resposta').append(`
                <div>${resp.username}</div>
            `);        
    }    


});
