<?php
	require('../secure/login.php');

	$conn = mysqli_connect($servername, $username, $password, $dbname);

	if(!$conn){
		die('Error de Conexion: '. mysqli_connect_errno());
	}

	$idusuari = $_POST['idusuari'];
	

	$query = "SELECT * FROM enquesta ORDER BY data_final DESC";
	$respondida = "";

	$resultado = mysqli_query($conn, $query);

	if(!$resultado){
		die("Error");
	}
	else{
		while($data = mysqli_fetch_object($resultado)) {
			if($stmt = mysqli_prepare($conn, "SELECT id_enquesta AS id, valor FROM resposta WHERE  id_usuari = ? AND  id_enquesta = ?")){
				mysqli_stmt_bind_param($stmt, "ii", $idusuari, $data->id);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_bind_result($stmt, $data->contestada, $data->valor);
				mysqli_stmt_fetch($stmt);
			}
			mysqli_stmt_close($stmt);

			$respuestas[]= $data;
	    }
	}
	
	print json_encode($respuestas);


	mysqli_free_result($resultado);
	mysqli_close($conn);



?>
