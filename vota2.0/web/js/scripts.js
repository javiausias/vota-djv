$(function () {
    $.ajax({
        type: 'post',
		url:'php/enquestes.php',
        dataType: 'JSON'
		}).done(function(datos){
			$.each(datos.data, function(index, itemData){
            	$('#resposta').append(`
					<div class='enquesta' >
						<div class="column">
							<h4 class='pregunta'>${itemData.pregunta}</h4>
							<div class="row">
								<div class="info">
									<p class='dataInici'>Inici: ${itemData.data_inici}</p>
									<p class='dataFinal'>Expira: ${itemData.data_final}</p>
								</div>
								<div  class="info">
									<p class='dataInici'>Si: ${itemData.si}</p>
									<p class='dataFinal'>No: ${itemData.no}</p>
								</div>
							</div>
						</div>
                        <div class='quesitoInici'><canvas id="${itemData.id}"></canvas></div>
					</div>
					<hr>
				`);

				if(itemData.si == undefined && itemData.no == undefined){
					pintaQueso(itemData.id, 0, 0);
				}else if(itemData.si == undefined){
					pintaQueso(itemData.id, 0, itemData.no);
				}else if(itemData.no == undefined){
					pintaQueso(itemData.id, itemData.si, 0);
				}else
					pintaQueso(itemData.id, itemData.si, itemData.no);
             });
    });
   

    //funcion que se ejecuta cuando haces submit
    $('form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'post',
                url: 'php/registre_usuari.php',
                data: $('form').serialize(),
                dataType: 'json',
           		success: function (response) {
            	//condiciones para derivar a la parte admin o a la parte usuario
                    if(response.admin == 1){
               			 //redireccionamiento
                        window.location.href ="/admin";
                    }
                    else{
	                    //Aqui ocultamos el formulario una vez hecho submit y comprobado que el usuario se encuentra en la BBDD 
	                    $('form').hide();
	                    $('#container').html('<button class="btn" id="logout">Tanca la sessio</button>');
	                    //Invocacion de la funcion para pintar todas las encuestas si no es admin, pasando por parametro el objeto usuario.
                        llamada(response);

                        $('#logout').on('click', function(e){
							e.preventDefault();
							$.ajax({
								type: 'POST',
								url: 'php/logout.php',
								success: function(dades){
									console.log(dades);
									window.location.href ="/";
								}
							});
						});
                    }
                }
            });


    });
    
    //funcion que es invocada si el usuario no es admin, que recibe por parametro el objeto usuario
    function llamada(resp){
    	obj = {
			idusuari: resp.id
    	}
        //En el div resposta pintaremos dentro lo que necesitemos
    	$.ajax({
           type: 'post',
           url:'php/totesEnquestes.php',
           data: obj,
           dataType: 'JSON'
		}).done(function(datos){
			pintaEnquestes(datos, resp);
		});
	}
    
    //funcion que recibe un objeto con todas enquestas y se encarga de pintarlas
    function pintaEnquestes(datos, resp){
    	$('#resposta').html("<div class='contesta'></div><div class='quesito'></div>");
		let hoy = new Date(new Date().toJSON().slice(0,10));
    	
    	$.each(datos, function(index, itemData){
    		//console.log(itemData);
    		let destacada = "";
    		let activa = "";
    		let aviso = "";
    		let inactiva = 0;
    		let respondida = "";

    		//Se comprueba las diferentes condiciones para poner los iconos correspondientes
    		if(itemData.destacada == 1){
				destacada = "img/fav.png";
           	}else{
				destacada = "img/nofav.png";
           	}
           	if(itemData.contestada == itemData.id){
           		respondida = "img/check.png";
           	}
           	let data = new Date(itemData.data_final);
			let fechaLimite = new Date();
			fechaLimite.setDate(fechaLimite.getDate() + 3);

			if(data.getTime() >= hoy.getTime()){
				activa = "img/clock.png";
				if(data <= fechaLimite){
					aviso ="img/warning.png";
				}
			}else{
				activa = "img/stopwatch.png";
				inactiva = 1;
			}

        	$('#resposta').append(`
				<div class='enquesta' id="${itemData.id}" inactiva="${inactiva}">
        			<div class='pregunta'>${itemData.pregunta}</div>
           			<div class='dataFinal'>${itemData.data_final}</div>
           			<div class='icons'>
           				<div class='avisoCierre'><img class="aviso" src="${aviso}"></img></div>
						<div class='respondida'><img class="respondida" src="${respondida}"></div>	
               			<div class='activa'><img class="activa" src="${activa}"></img></div>
               			<div class='destacada'><img class="destacada" src="${destacada}" alt="destacada"></img></div>
					</div>
       			</div>
			`);
		});

		//Al hacer click sobre una enquesta se quita el div con las enquestas y se muestra las opciones de la enquesta
		$(".enquesta").on('click',function(){
			$(".enquesta").remove();

			let id_enquesta = $(this).attr("id");
			$(".quesito").html("<canvas id='canvas'></canvas>");
			let id_canvas = $('#canvas').attr('id');
			let inactiva = $(this).attr("inactiva");

			//Si ha expirado la fecha no se puede contestar y se muestra el quesito directamente
			if(inactiva == 1){
				$(".contesta").remove();
				let obj = {
					idenquesta: id_enquesta
				}
				muestraQuesito(obj, id_canvas, resp);

			}else{
				$(".contesta").html($(this).children(".pregunta").html());
				$(".contesta").append("<form id='sino'>"
					+ "<input class='radioButton' type='radio' name='opcions' value='1'> Si<br>"
					+ "<input class='radioButton' type='radio' name='opcions' value='0'> No<br>"
					+ "</form>");

				//Cuando se hace click en los radio button se crea un objeto que se le pasa mediante ajax para hacer la query
				$("input[type=radio][name=opcions]").on('change', function(){
					$(".contesta").remove();
					let sino = $('#sino').serialize().split("=")[1];
					let obj = {
						idenquesta: id_enquesta,
						idusuari: resp.id,
						resposta: sino
					};
					muestraQuesito(obj, id_canvas, resp);
				});
			}
		});    
	}
    
	//funcion que consulta el numero de si y no de la enquesta y pinta el quesito
    function muestraQuesito(obj, id_canvas, resp){
		$.ajax({
			type: 'POST',
			url:'php/sisinos.php',
			data: obj,
			dataType: 'JSON',
			success: function(datos){
				$(".quesito").prepend('<p>Ha expirat el temps!</p>');

				if(datos.si == undefined && datos.no == undefined){
					pintaQueso(id_canvas, 0, 0);
				}else if(datos.si == undefined){
					pintaQueso(id_canvas, 0, datos.no);
				}else if(datos.no == undefined){
					pintaQueso(id_canvas, datos.si, 0);
				}else{
					pintaQueso(id_canvas, datos.si, datos.no);
				}
				$(".quesito").append('<button class="btn" id="enrere">Tornar enrere</button>');
				$(".quesito").show();

				$("#enrere").on("click", function(){
					$(".quesito").remove();
					llamada(resp);
    			});	
			}
		});
    }	   
    
    //Si pulsem logout en la part admin, tanca la sessio i ens retorna al inici    
	$('#logout-admin').on('click', function(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: '../../php/logout.php',
			success: function(dades){
				//console.log(dades);
				window.location.href ="/";
			}
		});
	});

});
