$(document).ready(function(){
    //Si pulsem logout en la part admin, tanca la sessio i ens retorna al inici    
	$('#logout').on('click', function(e){
		e.preventDefault();
		$.ajax({
			type: 'POST',
			url: "php/logout.php",
			success: function(dades){
				//console.log(dades);
				window.location.href ="/";
			}
		});
	});
});
